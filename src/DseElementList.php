<?php

/**
 * 361GRAD Element List
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementList;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Element List.
 */
class DseElementList extends Bundle
{
}
